package utility;

/**
 * @author Simone
 * The Enum with the name of each Bar, need to add ".png"
 */
public enum BarRes {
    /**
     * 
     */
    SAETTABAR,
    /**
     * 
     */
    RAINBOWBAR,
    /**
     * 
     */
    PLAYERBAR,
    /**
     * 
     */
    SWORDBAR,
    /**
     * 
     */
    CROCEBAR,
    /**
     * 
     */
    GOLDENBAR;

}
