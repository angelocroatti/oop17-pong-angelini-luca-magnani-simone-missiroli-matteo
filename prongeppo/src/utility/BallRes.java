package utility;

/**
 * @author Simone
 * The Enum with the name of each Ball, need to add ".png"
 */
public enum BallRes {

    /**
     * 
     */
    AVENGERS,
    /**
     * 
     */
    CHROME,
    /**
     * 
     */
    ORANGEBALL,
    /**
     * 
     */
    SAYANBALL,
    /**
     * 
     */
    DEATHSTAR,
    /**
     * 
     */
    SOCCERBALL;
}
